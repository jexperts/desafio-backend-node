# Desafio Back-End Node Developer

Este é um desafio prático para Node developers que desejam entrar para o nosso time.

O desafio consiste na criação de um aplicação web simples para cadastro de usuários (CRUD) com testes automatizados.

## Modelo de dados:

-   Nome (obrigatório, mínimo 3 caracteres e máximo 50 caracteres)
-   Telefone (opcional)
-   CPF (obrigatório, único e cpf válido)
-   Email (obrigatório e deve ser um email válido e único)
-   Login (obrigatório, único, mínimo 3 caracteres e máximo 20 caracteres)
-   Senha (obrigatório, persistir criptografada com hash e possuir o mínimo de 8 caracteres e máximo 30 caracteres, possuir letras maiúsculas e minúsculas e pelo menos um caracter especial)

## Pré-requisitos

-	REST API de cadastro, edição, listagem com paginação, listagem por nome, busca por id, e exclusão por id;
-	Mínimo Node 10;
-   Utilização ES6 ou ES7;
-	MongoDB;
-   Teste automatizado de todos os endpoints da API;
-   O projeto deve ser publicado em um repositório público do Github/Bitbucket/Gitlab (crie uma se você não possuir).

## Desejável

-   Analise de código com ESlint
-   Melhore a qualidade do código estendendo algum style guide

## O que esperamos:

-   Utilização de boas práticas
-   Técnicas de clean code
-   Que o app suba apenas com o comando: yarn start
-   Que o app rode os testes apenas com o comando: yarn test

## Plus (Opcional)

Publicar a aplicação em algum serviço como heroku, aws, google cloud com a documentação REST API com o Swagger.

## Plus Plus (Opcional)

Fazer o Front-end em React.js.